package cn.xlbweb.cli.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: bobi
 * @date: 2019-02-11 16:02
 * @description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "cn.xlbweb.cli")
public class CliProperties {

    private String jwtSign;
    private String excludeUrls;
}
