package cn.xlbweb.cli.exception;

import cn.xlbweb.cli.server.ResponseCode;
import cn.xlbweb.cli.server.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestControllerAdvice
@Slf4j
public class CliExceptionHandler {

    @ExceptionHandler(value = {AuthorizationException.class})
    public ServerResponse authorizationException(Exception e) {
        log.warn(e.getMessage());
        return ServerResponse.error(ResponseCode.UN_AUTH.getCode(), ResponseCode.UN_AUTH.getMsg());
    }
}
