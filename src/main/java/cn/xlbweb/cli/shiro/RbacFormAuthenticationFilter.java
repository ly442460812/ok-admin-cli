package cn.xlbweb.cli.shiro;

import cn.xlbweb.cli.server.ResponseCode;
import cn.xlbweb.cli.server.ServerResponse;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: bobi
 * @date: 2019-09-16 20:21
 * @description:
 */
@Slf4j
public class RbacFormAuthenticationFilter extends FormAuthenticationFilter {

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            return true;
        }
        return super.isAccessAllowed(request, response, mappedValue);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        log.warn("用户未登录");
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        ServerResponse serverResponse = ServerResponse.error(ResponseCode.UN_LOGIN.getCode(), ResponseCode.UN_LOGIN.getMsg());
        httpServletResponse.getWriter().write(JSONObject.toJSON(serverResponse).toString());
        return false;
    }
}
